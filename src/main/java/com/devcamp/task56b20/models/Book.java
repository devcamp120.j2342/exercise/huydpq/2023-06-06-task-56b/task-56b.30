package com.devcamp.task56b20.models;

import java.util.ArrayList;

public class Book {
    private String name;
    private double price;
    private int qty = 0;
    private ArrayList <Author> author;

    public Book() {
    }

    public Book(String name, double price, ArrayList<Author> author) {
        this.name = name;
        this.price = price;
        this.author = author;
    }

    public Book(String name, double price, int qty, ArrayList<Author> author) {
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Author> getAuthor() {
        return author;
    }

    public String steAuthorNames(){
        StringBuilder names = new StringBuilder();
        for (Author author : author) {
            names.append(author.getName()).append(", ");
        }
        return names.toString();

    }

    

    @Override
    public String toString() {
        return "Book [name=" + name + ", price=" + price + ", qty=" + qty + ", author=" + author + "]";
    }

}
