package com.devcamp.task56b20.Controller;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b20.models.Author;
import com.devcamp.task56b20.models.Book;

@RestController
public class BookAuthorAPI {
    @GetMapping("/books")
    public ArrayList<Book> getBooks() {
        // Khởi tạo 6 đối tượng tác giả
        Author author1 = new Author("Author 1", "author1@gmail.com", 'm');
        Author author2 = new Author("Author 2", "author2@gmail.com", 'f');
        Author author3 = new Author("Author 3", "author3@gmail.com", 'm');
        Author author4 = new Author("Author 4", "author4@gmail.com", 'f');
        Author author5 = new Author("Author 5", "author5@gmail.com", 'm');
        Author author6 = new Author("Author 6", "author6@gmail.com", 'f');

        // In thông tin 6 đối tượng tác giả ra console
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);

        // Khởi tạo 3 ArrayList Author mới và thêm các tác giả vào từng ArrayList
        ArrayList<Author> authorList1 = new ArrayList<>();
        authorList1.add(author1);
        authorList1.add(author2);

        ArrayList<Author> authorList2 = new ArrayList<>();
        authorList2.add(author3);
        authorList2.add(author4);

        ArrayList<Author> authorList3 = new ArrayList<>();
        authorList3.add(author5);
        authorList3.add(author6);

        // Khởi tạo 3 đối tượng sách tương ứng với các list tác giả vừa tạo
        Book book1 = new Book("Book 1", 20.5, 10, authorList1);
        Book book2 = new Book("Book 2", 15.0, 20, authorList2);
        Book book3 = new Book("Book 3", 25.0, 15, authorList3);

        // In thông tin 3 đối tượng sách ra console
        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        // Khởi tạo một ArrayList Book mới và thêm các đối tượng sách vừa tạo vào
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);

        // Trả về ArrayList Book
        return bookList;

    }

}
